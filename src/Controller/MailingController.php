<?php

namespace App\Controller;

use App\Entity\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MailingController extends Controller
{
    /**
     * @Route("/mailing", name="mailing")
     */
    public function mailing()
    {
        $mailing = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findAll();

        return $this->render('mailing/mailing.html.twig', ['mailing' => $mailing]
        );
    }
}
