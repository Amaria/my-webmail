<?php

namespace App\Controller;

use App\Entity\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReceptionController extends Controller
{
    /**
     * @Route("/reception", name="reception")
     */
    public function reception()
    {
        $reception = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findAll();

        return $this->render('reception/reception.html.twig', ['reception' => $reception]
        );
    }
}
