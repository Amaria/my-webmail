<?php

namespace App\Controller;

use App\Entity\Email;
use App\Form\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/email", name="emails")
 */
class EmailController extends Controller
{
    /**
     * @Route("", name="email")
     */
    public function index()
    {
        return $this->render('email/index.html.twig');
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request)
    {

        $email = new Email();

        $form = $this->createForm(EmailType::class, $email);


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();
        }


        return $this->render('email/new.html.twig', [
            'form' => $form->createView()
        ]);


    }

    /**
     * @Route("/response", name="response")
     */
    public function response(Request $request)
    {

        $email = new Email();

        $form = $this->createForm(EmailType::class, $email);


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();
        }


        return $this->render('email/new.html.twig', [
            'form' => $form->createView()
        ]);


    }


    /**
     * @Route("/{id}", name="show")
     */
    public function mailById($id)
    {

        $data = $this->getDoctrine()
            ->getRepository(Email::class)
            ->find($id);

        return $this->render('email/mailById.html.twig', ['data' => $data]);

    }


    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(Request $request, Email $email)
    {
        /*var_dump($email);*/

        $form = $this->createForm(EmailType::class, $email)
            ->add('save', SubmitType::class, array('label' => 'Editer'));


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();
        }


        return $this->render('email/edit.html.twig', [
            'id' => $email->getId(),
            'data' => $email,
            'form' => $form->createView()
        ]);


    }

    /**
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(Email $email){
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();

        return $this->redirect('/reception');
    }


    /**
     * @Route("/{id}/delete_mailing", name="delete_mailing")
     */
    public function deleteMailing(Email $email){
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();

        return $this->redirect('/mailing');
    }

}

