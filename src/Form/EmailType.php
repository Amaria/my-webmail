<?php

namespace App\Form;

use App\Entity\Email;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class EmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sender', TextType::class, array('label' => 'Expediteur'))
            ->add('recipient', TextType::class, array('label' => 'Destinataire'))
            ->add('subject', TextType::class, array('label' => 'Objet'))
            ->add('message', TextareaType::class, array('label' => 'Message'))
            ->add('date', DateType::class, array('label' => 'Date'))
            ->add('save', SubmitType::class, array('label' => 'Envoyer'));
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Email::class,
        ]);
    }


}
